#!/bin/bash
## define tools to install
tools=(
  tree
  vault
  fish
  python3
  git
  openssl
  docker
  docker-compose
  docker-machine
  gnu-sed
  pssh
  gpg2
  node
  tmux
  openldap
  nmap
  asciinema
  jwt-cli
  jq
  yq
  tldr
  watch
  links
  geoip
  bash-completion
  shellcheck
  mas
  kubectl
  kubernetes-helm
  rclone
  bash
  postgresql
  ubuntu/microk8s/microk8s
  fzf
  git-standup
)

## define apps to install
apps=(
  docker
  google-chrome
  firefox
  slack
  notion
  github
  postman
  cyberduck
  wireshark
  angry-ip-scanner
  vnc-viewer
  spotify
  visual-studio-code
  iterm2
  vlc
  telegram
  appcleaner
  vagrant
  cheatsheet
  powershell
  qlcolorcode
  qlmarkdown
  qlstephen
  zoomus
  teamviewer
  mqttfx
  macs-fan-control
  coconutbattery
  balenaetcher
  adobe-creative-cloud
  a-better-finder-attributes
  mysides
  keepassxc
  macmediakeyforwarder
  controlplane
  rectangle
  drawio
)

## define apps to be installed by pip
pip=(
  ansible
  passlib
)

## define apps to be installed from the mac appstore
appstore=(
  'spark email'
  'remote desktop 10'
)

## define VS Code plugins
vsc_plugins=(
  eamodio.gitlens
  emilast.LogFileHighlighter
  donjayamanne.githistory
  bbenoist.shell
  christian-kohler.path-intellisense
  CoenraadS.bracket-pair-colorizer-2
  DavidAnson.vscode-markdownlint
  donjayamanne.githistory
  esbenp.prettier-vscode
  kiteco.kite
  ms-azuretools.vscode-docker
  ms-kubernetes-tools.vscode-kubernetes-tools
  ms-python.python
  ms-vscode-remote.remote-containers
  ms-vsliveshare.vsliveshare
  oderwat.indent-rainbow
  PKief.material-icon-theme
  DavidAnson.vscode-markdownlint
  IBM.output-colorizer
  redhat.vscode-yaml
  samuelcolvin.jinjahtml
  vscoss.vscode-ansible
  ms-vscode-remote.remote-ssh
  ms-vscode-remote.remote-ssh-edit
)

## define whats in the dock from left to right
# Apps
dock_apps=(
  '/Applications/Safari'
  '/Applications/Google Chrome'
  '/Applications/Spark'
  '/Applications/Slack'
  '/Applications/Telegram'
  '/Applications/KeePassXC'
  '/Applications/Notion'
  '/System/Applications/Calendar'
  '/Applications/GitHub Desktop'
  '/Applications/Postman'
  '/Applications/Cyberduck'
  '/Applications/Angry IP Scanner'
  '/Applications/Wireshark'
  '/Applications/VNC Viewer'
  '/Applications/Spotify'
  '/System/Applications/App Store'
  '/Applications/Visual Studio Code'
  '/Applications/iTerm'
  '/System/Applications/System Preferences'
)
# Folders
dock_folders=(
  /Users/$USER/Documents
  /Users/$USER/Downloads
  /Applications
)

## define menu bar items 
menu_bar=(
  Bluetooth
  AirPort
  Battery
  Clock
  Displays
  TimeMachine
  User
  Volume
)

## define Favorites in Finder sidebar
finder_favorites=(
  'Applications file:///Applications/'
  'Downloads file:///Users/$USER/Downloads/'
  'Movies file:///Users/$USER/Movies/'
  'Music file:///Users/$USER/Music/'
  'Pictures file:///Users/$USER/Pictures/'
  '$USER file:///Users/$USER/'
)

## setup passwordless sudo
function setup_passwordless_sudo {
  if sudo cat /etc/sudoers | grep "$(whoami)    ALL=(ALL) NOPASSWD: ALL" >> /dev/null; then
    echo "Password less sudo already possible.";
  else
    printf "\n# Added via setup script\n$(whoami)    ALL=(ALL) NOPASSWD: ALL\n" \
    | sudo EDITOR='tee -a' visudo >> /dev/null
    echo "Added $(whoami) as passwordless sudoer";
  fi
}

## setup remote login
function setup_remote_login {
  if [ "$(sudo systemsetup -getremotelogin | awk -F: '{gsub(/ /, "", $0); print $2}')" == "Off" ]; then
    sudo systemsetup -setremotelogin on \
    && echo "Remote login enabled" \
    && sudo systemsetup -getremotelogin;
  else
    echo "Remote login already enabled." \
    && sudo systemsetup -getremotelogin;
  fi
}

## install homebrew
function install_homebrew {
  if brew help >> /dev/null; then
    printf "Homebrew already installed."
  else
    /usr/bin/ruby -e \
      "$(curl -fsSL \
      https://raw.githubusercontent.com/Homebrew/install/master/install)"
  fi
}

## install tools
function install_tools {
  brew tap mike-engel/jwt-cli
  for i in "${tools[@]}"; do
    brew install $i
  done
}

## upgrade tools
function upgrade_tools {
  for i in "${tools[@]}"; do
    brew upgrade $i
  done
}

## install apps
function install_apps {
  for i in "${apps[@]}"; do
    brew cask install $i
  done
}

## upgrade tools
function upgrade_apps {
  for i in "${apps[@]}"; do
    brew cask upgrade $i
  done
}

## install pip apps
function install_pip_apps {
  sudo easy_install pip
  for i in "${pip[@]}"; do
    sudo pip install $i
  done
}

## install mac appstore apps
function install_mac_apps {
  brew install mas
  for i in "${appstore[@]}"; do
    mas install $(
      mas search "$i" \
      | awk '{printf $1}'
    )
  done
}

## setup fish shell
function setup_fish {
  # General fish setup
  brew tap homebrew/cask-fonts
  brew cask install font-source-code-pro
  brew install fish
  if fish -c 'omf' >> /dev/null; then
    echo "Oh my fish already installed.";
  else
    curl -L https://get.oh-my.fish | fish;
  fi
  if cat /etc/shells | grep /usr/local/bin/fish >> /dev/null; then
    echo "Fish already in shells.";
  else
    sudo bash -c 'printf /usr/local/bin/fish >> /etc/shells';
  fi
  if [ $SHELL == "/usr/local/bin/fish" ]; then
    echo "Standard shell is fish.";
  else
    chsh -s /usr/local/bin/fish
  fi
  if [ $(fish -c "omf list" | awk /bobthefish/'{print $1}') == bobthefish ]; then
    echo "bobthefish already installed.";
  else
    fish -c "omf install bobthefish";
  fi
  # Install completions
  mkdir -p ~/.config/fish/completions
  if [ -f ~/.config/fish/completions/kubectl.fish ]; then
    echo "kubectl completion already installed.";
  else
    curl -s https://raw.githubusercontent.com/evanlucas/fish-kubectl-completions/master/completions/kubectl.fish \
      -o ~/.config/fish/completions/kubectl.fish \
    && echo "kubectl completion installed.";
  fi
  if [ -f ~/.config/fish/completions/docker.fish ]; then
    echo "docker completion already installed.";
  else
    curl -s https://raw.githubusercontent.com/docker/cli/master/contrib/completion/fish/docker.fish \
      -o ~/.config/fish/completions/docker.fish \
    && echo "docker completion installed.";
  fi
}

## install vs code plugins
function install_vsc_plugins {
  for i in "${vsc_plugins[@]}"; do
    code --install-extension $i --force
  done
}

## setup vs code
function setup_vs_code {
  cp ./vs_code/settings.json /Users/$USER/Library/ApplicationSupport/Code/User/settings.json
  install_vsc_plugins
}

## setup microk8s
function setup_microk8s {
  microk8s install -y
  microk8s enable \
    dns \
    helm3 \
    ingress \
    storage
  mkdir -p ~/.kube
  microk8s config > ~/.kube/config
}

## dock setup
function setup_dock {
  defaults write com.apple.dock persistent-apps -array
  defaults write com.apple.dock persistent-others -array
  for i in "${dock_apps[@]}"; do
      defaults write com.apple.dock persistent-apps -array-add "\
      <dict>
        <key>tile-data</key>
        <dict>
          <key>file-data</key>
          <dict>
            <key>_CFURLString</key>
            <string>$i.app</string>
            <key>_CFURLStringType</key>
            <integer>0</integer>
          </dict>
        </dict>
      </dict>"
  done
  for i in "${dock_folders[@]}"; do
      defaults write com.apple.dock persistent-others -array-add "\
      <dict>
        <key>tile-data</key>
        <dict>
          <key>file-data</key>
          <dict>
            <key>_CFURLString</key>
            <string>$i</string>
            <key>_CFURLStringType</key>
            <integer>0</integer>
          </dict>
        </dict>
      </dict>"
  done
  defaults write com.apple.dock tilesize -int 35
  defaults write com.apple.dock magnification -bool true
  defaults write com.apple.dock largesize -int 40
  defaults write com.apple.dock show-recents -bool FALSE
  killall Dock
}

## Menu bar settings
function setup_menu_bar {
  defaults write com.apple.menuextra.battery ShowPercent -string "YES"
  bash -c '> ~/Library/Preferences/com.apple.systemuiserver.plist'
  for i in "${menu_bar[@]}"; do
      defaults write com.apple.systemuiserver menuExtras -array \
        "/System/Library/CoreServices/Menu Extras/$i.menu"
  done
}

## Trackpad settings
function setup_trackpad {
  defaults write com.apple.AppleMultitouchTrackpad TrackpadThreeFingerDrag -bool true
  defaults write com.apple.AppleMultitouchTrackpad Clicking -bool true
}

## Finder settings
function setup_finder {
## Todo: Fix sidebar setup
#  mysides remove all
#  for i in "${finder_favorites[@]}"
#  do
#      mysides add $i
#  done
  defaults write com.apple.finder ShowExternalHardDrivesOnDesktop -bool true
  defaults write com.apple.finder ShowHardDrivesOnDesktop -bool true
  defaults write com.apple.finder ShowRemovableMediaOnDesktop -bool true
  defaults write com.apple.finder ShowMountedServersOnDesktop -bool true
  killall -HUP Finder
}

## Reboot with user confirmation
function reboot {
  read -p "Do you want to reboot now? (y/n)" -n 1 -r
  if [[ $REPLY =~ ^[Yy]$ ]]
  then
      printf "\nRebooting now..."
      sleep 5
      sudo reboot now
  fi
} 

## do install
function install_all {
  setup_passwordless_sudo
  setup_remote_login
  install_homebrew
  install_tools
  install_apps
  install_pip_apps
  install_mac_apps
  install_vsc_plugins
  setup_dock
  setup_menu_bar
  setup_trackpad
  setup_finder
  setup_fish
  reboot
}

case "$1" in
  setup_passwordless_sudo)
    setup_passwordless_sudo
    ;;
  setup_remote_login)
    setup_remote_login
    ;;
  install_homebrew)
    install_homebrew
    ;;
  install_tools)
    install_tools
    ;;
  upgrade_tools)
    upgrade_tools
    ;;
  install_apps)
    install_apps
    ;;
  upgrade_apps)
    upgrade_apps
    ;;
  install_pip_apps)
    install_pip_apps
    ;;
  install_mac_apps)
    install_mac_apps
    ;;
  install_vsc_plugins)
    install_vsc_plugins
    ;;
  setup_fish)
    setup_fish
    ;;
  setup_dock)
    setup_dock
    ;;
  setup_menu_bar)
    setup_menu_bar
    ;;
  setup_trackpad)
    setup_trackpad
    ;;
  setup_finder)
    setup_finder
    ;;
  install_all)
    install_all
    ;;
  upgrade_all)
    upgrade_tools
    upgrade_apps
    install_vsc_plugins
    ;;
  *)
    printf $"Usage: $0 with one of the following options:
  setup_passwordless_sudo
  setup_remote_login
  install_homebrew
  install_tools
  upgrade_tools
  install_apps
  upgrade_apps
  install_pip_apps
  install_mac_apps
  install_vsc_plugins
  setup_fish
  setup_microk8s
  setup_dock
  setup_menu_bar
  setup_trackpad
  setup_finder
  install_all
  upgrade_all"
    exit 1
esac

## Manual Stuff ##
# - [ ] Setup Nightshift from sunset to sunrise
# - [ ] Set `Source Code Pro for Powerline` as font in iTerm2
